package csb.controller;

import static csb.CSB_PropertyType.REMOVE_ITEM_MESSAGE;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.Lecture;
import csb.gui.CSB_GUI;
import csb.gui.MessageDialog;
import csb.gui.LectureItemDialog;
import csb.gui.YesNoCancelDialog;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Victor Cheng
 */
public class LectureEditController {
    LectureItemDialog lid;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    public LectureEditController(Stage initPrimaryStage, Course course, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        lid = new LectureItemDialog(initPrimaryStage, course, initMessageDialog);
        messageDialog = initMessageDialog;
        yesNoCancelDialog = initYesNoCancelDialog;
    }

    // THESE ARE FOR LECTURE ITEMS
    
    public void handleAddLectureItemRequest(CSB_GUI gui) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showAddLectureItemDialog();
        
        // DID THE USER CONFIRM?
        if (lid.wasCompleteSelected()) {
            // GET THE LECTURE ITEM
            Lecture li = lid.getLecture();
            
            // AND ADD IT AS A ROW TO THE TABLE
            course.addLecture(li);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    
    public void handleEditLectureItemRequest(CSB_GUI gui, Lecture itemToEdit) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        lid.showEditLectureItemDialog(itemToEdit);
        
        // DID THE USER CONFIRM?
        if (lid.wasCompleteSelected()) {
            // UPDATE THE LECTURE ITEM
            Lecture li = lid.getLecture();
            int iteIndex= course.getLectureIndex(itemToEdit);
            course.setLecture(iteIndex, li);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }        
    }
    
    public void handleRemoveLectureItemRequest(CSB_GUI gui, Lecture itemToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yesNoCancelDialog.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_ITEM_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoCancelDialog.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            gui.getDataManager().getCourse().removeLecture(itemToRemove);
        }
    }
    
    public void handleMoveUpLectureItemRequest(CSB_GUI gui, Lecture itemToMove) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        { 
            int selectedItemIndex = (gui.getDataManager().getCourse().getLectureIndex(itemToMove));
            if(selectedItemIndex == 0)
            {
                 return;       
            }
            else
            {
                Lecture temp = (gui.getDataManager().getCourse().getLecture(selectedItemIndex - 1));
                course.setLecture((selectedItemIndex - 1), gui.getDataManager().getCourse().getLecture(selectedItemIndex));
                course.setLecture((selectedItemIndex), temp);
            }
        }
    }
    
    public void handleMoveDownLectureItemRequest(CSB_GUI gui, Lecture itemToMove) {
        CourseDataManager cdm = gui.getDataManager();
        Course course = cdm.getCourse();
        { 
            int selectedItemIndex = (gui.getDataManager().getCourse().getLectureIndex(itemToMove));
            int lectureSize = gui.getDataManager().getCourse().getLectureSize();
            if(selectedItemIndex == lectureSize)
            {
                 return;       
            }
            else
            {
                Lecture temp = (gui.getDataManager().getCourse().getLecture(selectedItemIndex + 1));
                course.setLecture((selectedItemIndex + 1), gui.getDataManager().getCourse().getLecture(selectedItemIndex));
                course.setLecture((selectedItemIndex), temp);
            }
        }
    }
}